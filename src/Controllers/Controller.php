<?php


namespace Scigeniq\Core\Controllers;


use Illuminate\Routing\Controller as IlluminateController;

class Controller extends IlluminateController
{
    use AjaxRedirectTrait;
}